package com.cerotid.bank.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cerotid.bank.app.entity.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Long> {
	public List<Customer> findByAddressStateCode(String addr);

}
