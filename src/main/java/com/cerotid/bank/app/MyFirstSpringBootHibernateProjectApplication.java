package com.cerotid.bank.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MyFirstSpringBootHibernateProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstSpringBootHibernateProjectApplication.class, args);
		
		/*
		 * EntityManagerFactory emf = Persistence.createEntityManagerFactory("");
		 * EntityManager em = emf.createEntityManager();
		 * 
		 * Customer customer = em.find(Customer.class, "TX");
		 */
	}

}
