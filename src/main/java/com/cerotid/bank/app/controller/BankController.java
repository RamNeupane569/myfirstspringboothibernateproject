package com.cerotid.bank.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cerotid.bank.app.entity.Customer;
import com.cerotid.bank.app.service.CustomerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j

@RestController
@RequestMapping("/customers")
public class BankController {

	@Autowired
	private CustomerService repo;

	@PostMapping("/add-customer")
	public String addCustomer(@RequestBody Customer customer) {
		log.warn("Add customer called");

		log.warn(customer.toString());
		repo.save(customer);
		log.warn("customer saved to the database");

		return "customer data successfully saved";
	}

	@RequestMapping("/navigateToAddCustomer")
	public ModelAndView navigateToAddCustomer(Customer customer) {

		ModelAndView modelView = new ModelAndView();
		modelView.addObject("object1", new Customer());
		modelView.setViewName("addCustomer");

		return modelView;
	}

	@GetMapping("/getCustomer/{stateCode}")
	public ModelAndView getCustomer(@PathVariable String stateCode) {
		log.warn("get Customer by state code");
		ModelAndView modelView = new ModelAndView("Retrieve");

		List<Customer> customers = (List<Customer>) repo.findByAddressStateCode(stateCode);

		log.warn("findaddresstatecode: " + customers.size());
		Customer customer = customers.get(0);
		modelView.addObject(customer);
		return modelView;
	}

	@RequestMapping("/navigateToShowCustomerInfo")
	public String showCustomerInfo(Customer customer) {

		return "showCustomerInfo";
	}
}
