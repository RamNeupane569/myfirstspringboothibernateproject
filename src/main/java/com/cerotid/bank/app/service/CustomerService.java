package com.cerotid.bank.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerotid.bank.app.dao.CustomerRepo;
import com.cerotid.bank.app.entity.Customer;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepo customerRepo;
	
	public Customer save(Customer customer) {
		return customerRepo.save(customer);
	}
	
	public List<Customer> findByAddressStateCode(String addr){
		return customerRepo.findByAddressStateCode(addr);
		
	}

}
